# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_04_05_130553) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "adminpack"
  enable_extension "pgagent"
  enable_extension "plpgsql"

  create_table "authors", force: :cascade do |t|
    t.string "last_name"
    t.string "first_name"
  end

  create_table "bdd_books", force: :cascade do |t|
    t.string "title"
    t.string "author"
    t.string "category"
    t.integer "year"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "books", force: :cascade do |t|
    t.bigint "publisher_id", null: false
    t.string "title"
    t.string "isbn"
    t.string "category"
    t.bigint "author_id", null: false
    t.integer "number_of_pages"
    t.string "short_description"
    t.integer "price"
    t.boolean "available"
    t.integer "year_of_publication"
    t.index ["author_id"], name: "index_books_on_author_id"
    t.index ["publisher_id"], name: "index_books_on_publisher_id"
  end

  create_table "people", force: :cascade do |t|
    t.string "name"
    t.integer "age"
    t.string "gender"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "publishers", force: :cascade do |t|
    t.string "name"
    t.string "address"
  end

  add_foreign_key "books", "authors"
  add_foreign_key "books", "publishers"
end
