class CreateBddBooks < ActiveRecord::Migration[6.0]
  def change
    create_table :bdd_books do |t|
      t.string :title
      t.string :author
      t.string :category
      t.integer :year

      t.timestamps
    end
  end
end
