require 'rails_helper'

RSpec.describe "bdd_books/new", type: :view do
  before(:each) do
    assign(:bdd_book, BddBook.new(
      :title => "MyString",
      :author => "MyString",
      :category => "MyString",
      :year => 1
    ))
  end

  it "renders new bdd_book form" do
    render

    assert_select "form[action=?][method=?]", bdd_books_path, "post" do

      assert_select "input[name=?]", "bdd_book[title]"

      assert_select "input[name=?]", "bdd_book[author]"

      assert_select "input[name=?]", "bdd_book[category]"

      assert_select "input[name=?]", "bdd_book[year]"
    end
  end
end
