require 'rails_helper'

RSpec.describe "bdd_books/show", type: :view do
  before(:each) do
    @bdd_book = assign(:bdd_book, BddBook.create!(
      :title => "Title",
      :author => "Author",
      :category => "Category",
      :year => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/Author/)
    expect(rendered).to match(/Category/)
    expect(rendered).to match(/2/)
  end
end
