require 'rails_helper'

RSpec.describe "bdd_books/index", type: :view do
  before(:each) do
    assign(:bdd_books, [
      BddBook.create!(
        :title => "Title",
        :author => "Author",
        :category => "Category",
        :year => 2
      ),
      BddBook.create!(
        :title => "Title",
        :author => "Author",
        :category => "Category",
        :year => 2
      )
    ])
  end

  it "renders a list of bdd_books" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Author".to_s, :count => 2
    assert_select "tr>td", :text => "Category".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
