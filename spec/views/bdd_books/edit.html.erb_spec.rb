require 'rails_helper'

RSpec.describe "bdd_books/edit", type: :view do
  before(:each) do
    @bdd_book = assign(:bdd_book, BddBook.create!(
      :title => "MyString",
      :author => "MyString",
      :category => "MyString",
      :year => 1
    ))
  end

  it "renders the edit bdd_book form" do
    render

    assert_select "form[action=?][method=?]", bdd_book_path(@bdd_book), "post" do

      assert_select "input[name=?]", "bdd_book[title]"

      assert_select "input[name=?]", "bdd_book[author]"

      assert_select "input[name=?]", "bdd_book[category]"

      assert_select "input[name=?]", "bdd_book[year]"
    end
  end
end
