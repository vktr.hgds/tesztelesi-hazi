require "rails_helper"

RSpec.describe BddBooksController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/bdd_books").to route_to("bdd_books#index")
    end

    it "routes to #new" do
      expect(:get => "/bdd_books/new").to route_to("bdd_books#new")
    end

    it "routes to #show" do
      expect(:get => "/bdd_books/1").to route_to("bdd_books#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/bdd_books/1/edit").to route_to("bdd_books#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/bdd_books").to route_to("bdd_books#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/bdd_books/1").to route_to("bdd_books#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/bdd_books/1").to route_to("bdd_books#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/bdd_books/1").to route_to("bdd_books#destroy", :id => "1")
    end
  end
end
