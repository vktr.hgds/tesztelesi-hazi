require 'selenium-webdriver'
require 'capybara'

Given /^Create a new Book object/ do
  @browser = Selenium::WebDriver.for :chrome
  @browser.navigate.to "http://localhost:3000/bdd_books/new"
end

When /^I fill in "([^"]*)" field with "([^"]*)"$/ do |field_name, value|
  @element = @browser.find_element(:name, field_name)
  @element.send_keys value
end

And /^I fill in "([^"]*)" field with "([^"]*)" too$/ do |field_name, value|
  @element = @browser.find_element(:name, field_name)
  @element.send_keys value
end

And /^I will fill in "([^"]*)" field with "([^"]*)"$/ do |field_name, value|
  @element = @browser.find_element(:name, field_name)
  @element.send_keys value
end

And /^I am going to fill in "([^"]*)" field with ([^"]*)$/ do |field_name, value|
  @element = @browser.find_element(:name, field_name)
  @element.send_keys value
end

And /^I will fill in "([^"]*)" field with ([^"]*)$/ do |field_name, value|
  @element = @browser.find_element(:name, field_name)
  @element.send_keys value
end

And /^finally, I click on submit button/ do
  @element.submit ? @message = "Successful" : @message = "Error"
end

Then /^I should get "Successful" message/ do
  @message == "Successful"
end