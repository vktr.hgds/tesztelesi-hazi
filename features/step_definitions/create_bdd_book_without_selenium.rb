require 'capybara'

# use Capybara library
Given /^Create without Selenium/ do
  visit "http://localhost:3000/bdd_books/new"
end

When /^I fill in all input field except for title/ do
  fill_in 'bdd_book[author]', :with => "Author Proba"
  fill_in 'bdd_book[category]', :with => "Sci-fi"
  fill_in 'bdd_book[year]', :with => 2010
end

And /^I click on submit button/ do
  find('[name=commit]').click
end

Then /^I should see "error" message/ do
  BddBook.last.title != ""
end