Feature: Create Book object

  Scenario: Fill in form
    Given Create a new Book object
    When I fill in "bdd_book[title]" field with "Proba Konyv"
    And I fill in "bdd_book[author]" field with "Author" too
    And I will fill in "bdd_book[category]" field with "Fantasy"
    And I am going to fill in "bdd_book[year]" field with 2010
    And finally, I click on submit button
    Then I should get "Successful" message


