Feature: Create Book object without Selenium

  Scenario: Fill in form
    Given Create without Selenium
    When I fill in all input field except for title
    And I click on submit button
    Then I should see "error" message

