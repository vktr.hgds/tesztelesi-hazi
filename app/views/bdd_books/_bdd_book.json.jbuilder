json.extract! bdd_book, :id, :title, :author, :category, :year, :created_at, :updated_at
json.url bdd_book_url(bdd_book, format: :json)
