class BddBooksController < ApplicationController
  before_action :set_bdd_book, only: [:show, :edit, :update, :destroy]

  # GET /bdd_books
  # GET /bdd_books.json
  def index
    @bdd_books = BddBook.all
  end

  # GET /bdd_books/1
  # GET /bdd_books/1.json
  def show
  end

  # GET /bdd_books/new
  def new
    @bdd_book = BddBook.new
  end

  # GET /bdd_books/1/edit
  def edit
  end

  # POST /bdd_books
  # POST /bdd_books.json
  def create
    @bdd_book = BddBook.new(bdd_book_params)

    respond_to do |format|
      if @bdd_book.save
        format.html { redirect_to @bdd_book, notice: 'Bdd book was successfully created.' }
        format.json { render :show, status: :created, location: @bdd_book }
      else
        format.html { render :new }
        format.json { render json: @bdd_book.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bdd_books/1
  # PATCH/PUT /bdd_books/1.json
  def update
    respond_to do |format|
      if @bdd_book.update(bdd_book_params)
        format.html { redirect_to @bdd_book, notice: 'Bdd book was successfully updated.' }
        format.json { render :show, status: :ok, location: @bdd_book }
      else
        format.html { render :edit }
        format.json { render json: @bdd_book.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bdd_books/1
  # DELETE /bdd_books/1.json
  def destroy
    @bdd_book.destroy
    respond_to do |format|
      format.html { redirect_to bdd_books_url, notice: 'Bdd book was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bdd_book
      @bdd_book = BddBook.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def bdd_book_params
      params.require(:bdd_book).permit(:title, :author, :category, :year)
    end
end
