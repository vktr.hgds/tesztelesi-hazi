Rails.application.routes.draw do
  resources :bdd_books
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root :to => 'bdd_books#new'
end
